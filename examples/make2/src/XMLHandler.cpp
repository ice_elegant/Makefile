#include "XMLHandler.h"
#include <iostream>
using namespace std;

CXML_Handler::CXML_Handler()
{

}

CXML_Handler::~CXML_Handler()
{

}

bool CXML_Handler::handle()
{
	string file_name = "db.xml";

	// 加载文件
	TiXmlDocument doc(file_name.c_str());
	bool loadResult = doc.LoadFile();
	if (!loadResult)
	{
		printf("Could not load file= %s, Error=%s\n",file_name.c_str(),doc.ErrorDesc());
		return false;
	}

	// 读取根目录
	TiXmlElement* root = doc.RootElement(); 

	for (TiXmlNode* item_node = root->FirstChild("item"); item_node != NULL; item_node = item_node->NextSibling("item"))
	{
		TiXmlNode* first_node = item_node->FirstChild();
		if (first_node != NULL)
		{
			const char* host_str = first_node->ToElement()->GetText();
			cout<<host_str<<endl;
		}

		TiXmlNode* port_node = item_node->IterateChildren(first_node);
		if (port_node != NULL)
		{
			const char* port_str = port_node->ToElement()->GetText();
			cout<<port_str<<endl;
		}
	}

	// 新建
	TiXmlElement* new_item_node = new TiXmlElement("item");

	TiXmlElement* host_element = new TiXmlElement("host");
	host_element->LinkEndChild(new TiXmlText("192.168.3.11"));
	TiXmlElement* port_element = new TiXmlElement("port");
	port_element->LinkEndChild(new TiXmlText("3308"));

	new_item_node->LinkEndChild(host_element);
	new_item_node->LinkEndChild(port_element);

	root->LinkEndChild(new_item_node);

	// 修改
	TiXmlNode* pItemNode = root->FirstChildElement("item");
	if (pItemNode)
	{
		TiXmlNode* pHostNode = pItemNode->FirstChildElement("host");
		if (pHostNode)
		{
			TiXmlNode* pHostValueNOde = pHostNode->FirstChild();
			if (pHostValueNOde)
			{
				pHostValueNOde->SetValue("modify 192.168.1.1");
			}
		}
	}

	// 删除最后一个
	TiXmlNode* pItemNode_Del = root->LastChild("item");
	if (pItemNode_Del)
	{
		TiXmlElement* pHostNode = pItemNode_Del->FirstChildElement("host");
		if (pHostNode)
		{
			pItemNode_Del->RemoveChild(pHostNode);
		}
	}

	// 保存
	doc.SaveFile();

	return true;
}