#include "JsonHandler.h"
#include "json/json.h"

CJsonHandler::CJsonHandler()
{
	a = 0;
}

CJsonHandler::~CJsonHandler()
{

}

bool CJsonHandler::handle()
{	
	// Value
	Json::Value json_temp;
	json_temp["name"]	= Json::Value("huchao");
	json_temp["age"]	= Json::Value(26);

	Json::Value root; 
	root["key_string"]	= Json::Value("value_string"); // 新建一个 Key(名为：key_string),赋予字符串值:”value_string”
	root["key_number"]	= Json::Value(12345);	// 新建一个 Key(名为:key_number),赋予数值:12345
	root["key_boolean"] = Json::Value(false);	// 新建一个 Key(名为:key_boolean),赋予bool值:false
	root["key_double"]	= Json::Value(12.345);	// 新建一个 Key(名为:key_double),赋予 double 值:12.345
	root["key_object"]	= json_temp;			// 新建一个 Key(名为:key_object)，赋予 json::Value 对象值。
	root["key_array"].append("array_string");	// 新建一个 Key(名为:key_array),类型为数组,对第一个元素赋值为字符串:”array_string”
	root["key_array"].append(1234);				// 为数组 key_array 赋值,对第二个元素赋值为:1234。
	Json::ValueType type = root.type();			// 获得 root 的类型，此处为 objectValue 类型。
	if (type == Json::objectValue)
	{
		cout<<"type is Json::objectValue\n"<<endl;
	}


	// Writer(用于从json输出)
	Json::FastWriter fast_writer;
	cout<<fast_writer.write(root)<<endl;

	Json::StyledWriter styled_writer;
	cout<<styled_writer.write(root)<<endl;

	//Reader(用于把字符串读入json)
	Json::Reader reader;
	Json::Value json_object;
	const char* json_document = "{\"age\":26,\"name\":\"huchao\"}";
	if (!reader.parse(json_document, json_object))
	{
		return 0;
	}
	std::cout << json_object["name"] << std::endl;
	std::cout << json_object["age"] << std::endl;
	return true;
}